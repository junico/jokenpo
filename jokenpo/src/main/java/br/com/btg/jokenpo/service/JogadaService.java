package br.com.btg.jokenpo.service;

import br.com.btg.jokenpo.enums.JogadaEnum;
import br.com.btg.jokenpo.model.Jogada;
import br.com.btg.jokenpo.model.Jogador;
import br.com.btg.jokenpo.repository.JogadaRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class JogadaService {

    private JogadaRepository jogadaRepository;
    private JogadorService jogadorService;

    public JogadaService(JogadaRepository jogadaRepository, JogadorService jogadorService) {
        this.jogadaRepository = jogadaRepository;
        this.jogadorService = jogadorService;
    }

    public Jogada cadastrarJogada(Integer id, JogadaEnum jogadaEnum){
        Jogador jogador = jogadorService.buscarPorId(id);

        Jogada jogada = Jogada.builder()
                .jogador(jogador)
                .jogadaEnum(jogadaEnum)
                .build();

        return jogadaRepository.save(jogada);
    }

    public List<Jogada> buscarTodos(){

        return jogadaRepository.findAll();
    }

    public void deletar(Integer id){
        jogadaRepository.deleteById(id);
    }
}
