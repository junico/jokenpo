package br.com.btg.jokenpo.repository;

import br.com.btg.jokenpo.model.Jogada;
import br.com.btg.jokenpo.model.Jogador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JogadaRepository extends JpaRepository<Jogada, Integer> {
}
