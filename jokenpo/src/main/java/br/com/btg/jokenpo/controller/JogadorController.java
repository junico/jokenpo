package br.com.btg.jokenpo.controller;

import br.com.btg.jokenpo.dto.JogadorRequest;
import br.com.btg.jokenpo.dto.JogadorResponse;
import br.com.btg.jokenpo.mapper.JogadorMapper;
import br.com.btg.jokenpo.model.Jogador;
import br.com.btg.jokenpo.service.JogadorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/jogador")
public class JogadorController {

    private JogadorService jogadorService;
    private JogadorMapper jogadorMapper;

    public JogadorController(JogadorService jogadorService, JogadorMapper jogadorMapper) {
        this.jogadorService = jogadorService;
        this.jogadorMapper = jogadorMapper;
    }

    @PostMapping
    private ResponseEntity<JogadorResponse> salvar(@RequestBody JogadorRequest request){

        Jogador jogador = jogadorService.salvar(request);

        JogadorResponse jogadorResponse = jogadorMapper.mapJogadorToJogadorResponse(jogador);

        return new ResponseEntity<>(jogadorResponse, HttpStatus.CREATED);
    }

    @GetMapping
    private ResponseEntity<List<JogadorResponse>> buscarTodos(){

        List<Jogador> jogadores = jogadorService.buscarTodos();

        List<JogadorResponse> jogadorResponse = jogadores.stream().map(jogadorMapper::mapJogadorToJogadorResponse).collect(Collectors.toList());

        return new ResponseEntity<>(jogadorResponse, HttpStatus.OK);
    }

    @GetMapping("/id/{id}")
    private ResponseEntity<JogadorResponse> buscarPorId(@PathVariable Integer id){

        Jogador jogador = jogadorService.buscarPorId(id);

        JogadorResponse jogadorResponse = jogadorMapper.mapJogadorToJogadorResponse(jogador);

        return new ResponseEntity<>(jogadorResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    private void deletar(@PathVariable Integer id){
        jogadorService.deletar(id);
    }

}
