package br.com.btg.jokenpo.mapper;

import br.com.btg.jokenpo.dto.JogadaResponse;
import br.com.btg.jokenpo.model.Jogada;
import org.springframework.stereotype.Component;

@Component
public class JogadaMapper {

    public JogadaResponse mapJogadaToJogadaResponse(Jogada jogada){
        return JogadaResponse.builder()
                .nome(jogada.getJogador().getNome())
                .jogada(jogada.getJogadaEnum())
                .build();
    }
}
