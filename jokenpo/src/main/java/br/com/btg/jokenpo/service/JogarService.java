package br.com.btg.jokenpo.service;

import br.com.btg.jokenpo.exception.JogadoresInsulficientesException;
import br.com.btg.jokenpo.model.Jogada;
import br.com.btg.jokenpo.model.Jogador;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class JogarService {

    private JogadaService jogadaService;
    private static Map<String, String> jogadas = new HashMap<>();

    public JogarService(JogadaService jogadaService) {
        this.jogadaService = jogadaService;
    }

    public String jogar(){
        List<Jogada> jogadas = jogadaService.buscarTodos();

        validaJogadores(jogadas);

        List<Jogador> invictos = new ArrayList<>();

        jogadas.stream()
                .filter(jogada -> jogadas.stream().noneMatch(jogadaComparar -> jogadaComparar.getJogadaEnum().getVenceDe().contains(jogada.getJogadaEnum())))
                .map(Jogada::getJogador)
                .forEach(invictos::add);


        return validaVitoria(invictos);
    }

    private void validaJogadores(List<Jogada> jogadas) {
        Optional.ofNullable(jogadas)
                .filter(jogada -> jogadas.size() > 1)
                .orElseThrow(() -> new JogadoresInsulficientesException("Quantidade de jogadores tem que ser no mínimo 2"));
    }

    private String validaVitoria(List<Jogador> invictos) {
        return invictos.size() == 0 || invictos.size() > 1 ? "Empate" : "Vitória jogador " + invictos.get(0).getNome();
    }
}
