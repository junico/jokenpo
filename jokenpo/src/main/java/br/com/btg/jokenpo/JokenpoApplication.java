package br.com.btg.jokenpo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class JokenpoApplication {

    public static void main(String[] args) {
        SpringApplication.run(JokenpoApplication.class, args);
    }

}
