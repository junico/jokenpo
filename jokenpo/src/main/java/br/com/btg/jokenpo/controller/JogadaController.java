package br.com.btg.jokenpo.controller;

import br.com.btg.jokenpo.dto.JogadaRequest;
import br.com.btg.jokenpo.dto.JogadaResponse;
import br.com.btg.jokenpo.mapper.JogadaMapper;
import br.com.btg.jokenpo.model.Jogada;
import br.com.btg.jokenpo.service.JogadaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/jogada")
public class JogadaController {

    private JogadaService jogadaService;
    private JogadaMapper mapper;

    public JogadaController(JogadaService jogadaService, JogadaMapper mapper) {
        this.jogadaService = jogadaService;
        this.mapper = mapper;
    }

    @PostMapping
    private ResponseEntity<JogadaResponse> salvar(@RequestBody JogadaRequest jogadaRequest){

        Jogada jogada = jogadaService.cadastrarJogada(jogadaRequest.getId(), jogadaRequest.getJogada());

        JogadaResponse jogadaResponse = mapper.mapJogadaToJogadaResponse(jogada);

        return new ResponseEntity<JogadaResponse>(jogadaResponse, HttpStatus.CREATED);
    }

    @GetMapping
    private ResponseEntity<List<JogadaResponse>> buscarTodos(){

        List<Jogada> jogadas = jogadaService.buscarTodos();

        List<JogadaResponse> jogadasResponse = jogadas.stream().map(mapper::mapJogadaToJogadaResponse).collect(Collectors.toList());

        return new ResponseEntity<>(jogadasResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    private void deletar(@PathVariable Integer id){
        jogadaService.deletar(id);
    }

}
