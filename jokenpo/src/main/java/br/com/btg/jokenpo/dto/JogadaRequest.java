package br.com.btg.jokenpo.dto;

import br.com.btg.jokenpo.enums.JogadaEnum;
import lombok.Data;

@Data
public class JogadaRequest {

    private Integer id;
    private JogadaEnum jogada;
}
