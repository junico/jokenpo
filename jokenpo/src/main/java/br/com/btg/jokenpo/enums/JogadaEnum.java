package br.com.btg.jokenpo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@Getter
public enum JogadaEnum {

    PEDRA(0, "Pedra"),
    PAPEL(1, "Papel"),
    TESOURA(2, "Tesoura"),
    LAGARTO(3, "Lagarto"),
    SPOCK(4, "Spock");

    private int codigo;
    private String descricao;
    private List<JogadaEnum> venceDe;

    JogadaEnum(int codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    static {
        PEDRA.setVenceDe(Arrays.asList(JogadaEnum.TESOURA, JogadaEnum.LAGARTO));
        PAPEL.setVenceDe(Arrays.asList(JogadaEnum.PEDRA, JogadaEnum.SPOCK));
        TESOURA.setVenceDe(Arrays.asList(JogadaEnum.PAPEL, JogadaEnum.LAGARTO));
        LAGARTO.setVenceDe(Arrays.asList(JogadaEnum.SPOCK, JogadaEnum.PAPEL));
        SPOCK.setVenceDe(Arrays.asList(JogadaEnum.TESOURA, JogadaEnum.PAPEL));
    }


    public void setVenceDe(List<JogadaEnum> venceDe) {
        this.venceDe = venceDe;
    }
}
