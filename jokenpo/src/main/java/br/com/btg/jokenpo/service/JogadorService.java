package br.com.btg.jokenpo.service;

import br.com.btg.jokenpo.dto.JogadorRequest;
import br.com.btg.jokenpo.model.Jogador;
import br.com.btg.jokenpo.repository.JogadorRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class JogadorService {

    private JogadorRepository jogadorRepository;

    public JogadorService(JogadorRepository jogadorRepository) {
        this.jogadorRepository = jogadorRepository;
    }

    public Jogador salvar(JogadorRequest jogador){

        Jogador dados = Jogador.builder()
                .nome(jogador.getNome())
                .build();

        return jogadorRepository.save(dados);

    }

    public List<Jogador> buscarTodos(){
        return jogadorRepository.findAll();
    }

    public Jogador buscarPorId(Integer id){
        return jogadorRepository.findById(id).get();
    }

    public void deletar(Integer id){
        jogadorRepository.deleteById(id);
    }
}
