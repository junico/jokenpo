package br.com.btg.jokenpo.dto;

import br.com.btg.jokenpo.enums.JogadaEnum;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JogadaResponse {

    private String nome;
    private JogadaEnum jogada;
}
