package br.com.btg.jokenpo.controller;

import br.com.btg.jokenpo.service.JogarService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/jogar")
public class JogarController {

    private JogarService jogarService;

    public JogarController(JogarService jogarService) {
        this.jogarService = jogarService;
    }


    @GetMapping
    private ResponseEntity<String> jogar(){

        String jogar = jogarService.jogar();

        return new ResponseEntity<>(jogar, HttpStatus.OK);
    }

}
