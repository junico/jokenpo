package br.com.btg.jokenpo.mapper;

import br.com.btg.jokenpo.dto.JogadaResponse;
import br.com.btg.jokenpo.dto.JogadorResponse;
import br.com.btg.jokenpo.model.Jogada;
import br.com.btg.jokenpo.model.Jogador;
import org.springframework.stereotype.Component;

@Component
public class JogadorMapper {

    public JogadorResponse mapJogadorToJogadorResponse(Jogador jogador){
        return JogadorResponse.builder()
                .nome(jogador.getNome())
                .build();
    }
}
