package br.com.btg.jokenpo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class JogadoresInsulficientesException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public JogadoresInsulficientesException(String errorMessage) {
        super(errorMessage);
    }
}


